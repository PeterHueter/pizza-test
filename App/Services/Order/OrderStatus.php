<?php
namespace App\Services\Order;

/**
 * Возможные статусы заказа
 */
class OrderStatus
{
    const NEWCREATED = 1; // новый заказ
    const INROUTE = 2; // Заказ в маршруте
}
