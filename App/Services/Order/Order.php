<?php

namespace App\Services\Order;

use App\Services\Logistic\Point;
use App\Services\Owen\Contracts\Bakeable;

class Order implements Bakeable
{
    /**
     * Id заказа
     * @var ште
     */
    public $id;

    /**
     * Время готовки в минутах
     * @var int
     */
    public $baketime;

    /**
     * Координаты доставки
     * @var Services\Logistic\Coord
     */
    public $destination;

    /**
     * Когда поступил заказ начиная с 0
     * @var int
     */
    public $created_at;

    /**
     * Статус заказа
     * @var Services\Order\OrderStatus
     */
    private $status;

    public function __construct($baketime, $x, $y)
    {
        $this->baketime = $baketime;
        $this->destination = new Point($x, $y);
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Время готовки
     */
    public function bakeTime(): int
    {
        return $this->baketime;
    }
}
