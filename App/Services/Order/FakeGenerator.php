<?php

namespace App\Services\Order;

/**
 * Генератор случайных заказов
 */
class FakeGenerator
{
    private $last_time = 0;
    /**
     * Сгенерировать данные для создания заказа
     * @return array
     */
    public function get(): array
    {
        return [
            'baketime' => rand(10, 30),
            'x' => rand(-1000, 1000),
            'y' => rand(-1000, 1000),
            'created_at' => $this->getTime(),
        ];
    }

    /**
     * Возвращает время от последнего события с интервалом 1-30
     * @return int
     */
    private function getTime(): int
    {
        $this->last_time += rand(1, 30);
        return $this->last_time;
    }
}
