<?php

namespace App\Services\Order\Repository;

use App\Services\Order\Order;
use App\Services\Order\OrderStatus;
use App\Services\Order\Repository\Contracts\Repository as OrderRepository;
use Exception;

/**
 * Репозиторий для хранения заказов в памяти
 */
class InMemory implements OrderRepository
{
    private $last_id = 0;
    private $data = [];

    public function store($baketime, $x, $y, $created_at = null): int
    {
        $this->last_id++;
        $order = new Order($baketime, $x, $y);
        $order->id = $this->last_id;
        $order->created_at = $created_at;
        $order->setStatus(OrderStatus::NEWCREATED);

        array_push($this->data, $order);
        return $this->last_id;
    }
    public function find($id): Order
    {
        foreach ($this->data as &$order) {
            if ($order->id === $id) {
                return $order;
            }
        }
        throw new Exception("Order with id {$id} not found");
    }
    public function delete($id): bool
    {
        foreach ($this->data as &$order) {
            if ($order->id === $id) {
                unset($order);
                return true;
            }
        }
        return false;
    }

    public function all(): array
    {
        return $this->data;
    }
}
