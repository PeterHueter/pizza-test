<?php
namespace App\Services\Order\Repository\Contracts;

use App\Services\Order\Order;

interface Repository
{
    /**
     * Добавить новый заказ в систему
     * @param int $baketime   Время готовки
     * @param int $x
     * @param int $y
     * @param int $createtime Время создания начиная от 0, если null, то берётся текущее время
     * @return int Id заказа
     */
    public function store($baketime, $x, $y, $createtime = null): int;

    /**
     * Найти заказ по Id
     * @param  int $id Заказа
     * @return Services\Order\Order
     * @throws Services\Order\Repository\Exceptions\OrderNotFound Заказ не найден
     */
    public function find($id): Order;

    /**
     * Удалить заказ
     * @param  int $id
     */
    public function delete($id): bool;

    /**
     * Возвращает все заказы в репозитория
     */
    public function all(): array;
}
