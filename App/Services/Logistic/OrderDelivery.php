<?php

namespace App\Services\Logistic;

use App\Services\Order\Order;

class OrderDelivery
{
    public $order;
    public $when;

    /**
     * [__construct description]
     * @param Order  $order Заказ для досатвки
     * @param int $when  Когда заказ должен быть доставлен
     */
    public function __construct(Order $order, int $when)
    {
        $this->order = $order;
        $this->when = $when;
    }
}
