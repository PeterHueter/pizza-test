<?php

namespace App\Services\Logistic;

/**
 * Стандартное средство доставки
 */
class Vehicle
{
    protected $speed = 60;

    /**
     * Время движения на определённое расстояние
     * @param  float $distance Растсояние
     * @return int Время
     */
    public function moveTime(float $distance): int
    {
        return $distance / $this->speed;
    }
}
