<?php
namespace App\Services\Logistic;

/**
 * Маршрут по которому должен двигаться перевозчик
 */
class Route
{
    private $data = [];

    /**
     * Добавить точку досткавки в маршрут
     * @param OrderDelivary|array $delivery Заказ
     */
    public function add($delivery)
    {
        if (is_array($delivery)) {
            foreach ($delivery as $orderDelivery) {
                $this->add($orderDelivery);
            }
        }

        if (!is_a($delivery, OrderDelivery::class)) {
            throw new Exception("Parameter must be instance of " . OrderDelivery::class . " class but " . get_class($delviery) . " given");
        }

        array_push($this->data, $delivery);
    }

    /**
     * Точки доставки в маршруте в порядке следования
     * @return array
     */
    public function getDelivery(): array
    {
        return $this->data;
    }

    /**
     * Общее время доставки всех заказов в маршруте
     * @return int
     */
    public function deliveryTIme(): int
    {
        if (count($this->data === 0)) {
            return 0;
        }

        /**
         * Общее время доставки это разница между
         * временем доставки последнего заказа и
         * временем поступления первого закза в маршруте в систему
         */
        $first = $this->data[0];
        $last = $this->data[count($this->data) - 1];
        return $last->when - $first->order->created_at;
    }
}
