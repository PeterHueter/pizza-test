<?php

namespace App\Services\Logistic;

/**
 * Координата
 */
class Point
{
    public $x;
    public $y;

    public function __construct($x = 0, $y = 0)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * Расстояние до заданой точки
     */
    public function distance(Point $dst): float
    {
        $dx = abs($this->x - $dst->x);
        $dy = abs($this->y - $dst->y);
        return sqrt($dx ** 2 + $dy ** 2);
    }
}
