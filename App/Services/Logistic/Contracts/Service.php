<?php

namespace App\Services\Logistic\Contracts;

interface Service
{
    /**
     * Рассчитать маршур доставки по заданным точкам
     * @param  array  $orders Список заказов для расчёта маршрута
     * @return array масссив из App\Services\Logistic\Contracts\Route
     */
    public function routes(array $orders): array;
}
