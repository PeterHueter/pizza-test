<?php

namespace App\Services\Logistic;

use App\Services\Logistic\Contracts\Service;
use App\Services\Order\Order;
use App\Services\Order\OrderStatus;
use App\Services\Owen\Owen;

/**
 *
 */
class SimpleService implements Service
{
    protected $inroute = 3;
    protected $max_delivery = 60;
    private $vehicle;
    private $owen;

    /**
     * Экземпляр сервиса
     * @param integer $max_delivery Максимальное время доставки каждого заказа
     */
    public function __construct(Vehicle $vehicle, Owen $owen)
    {
        $this->vehicle = $vehicle;
        $this->owen = $owen;
    }

    /**
     * Рассчитать маршрут доставки по заданным точкам
     * @param  array $orders Список заказов для расчёта маршрута
     * @return array масссив из App\Services\Logistic\Contracts\Route
     */
    public function routes(array $orders): array
    {
        // должен быть хотябы один заказ
        if (empty($orders)) {
            throw new Exception("Список заказов пуст");
        }

        $routes = [];

        while ($filtered = $this->filterOrders($orders)) {
            array_push($routes, $this->route($filtered));
        }
        return $routes;
    }

    /**
     * Сгенерировать маршрут
     * @param array $filtered Массив заказов которые могут быть доставлены за даный промежуток времени
     * @return App\Serivces\logistic\Route
     */
    private function route(array $filtered): Route
    {
        $owen = $this->owen;
        $route_orders = [];

        // первая точка маршрута - самый долгий заказ
        $scores = $this->scoreTable($owen, new Point(0, 0), $filtered);
        $order = reset($scores); // значение первго элемента

        // добавить первый из отфильтрованных заказов в путь
        array_push($route_orders, $order);
        $exclude = [$order->id];
        $curent = $order;

        // тестирование маршрута
        $test_route = $route_orders;

        // последующие заказы должны иметь минимальное время готовки
        // и расположены ближе к текущей точке маршрута
        for ($i = 0; $i < ($this->inroute - 1); $i++) {
            $scores = $this->scoreTable($owen, $curent->destination, $filtered, $exclude);

            if (empty($scores)) {
                break;
            }

            $order = end($scores);
            array_push($test_route, $order);

            // проверить если можно успеть по этому маршруту
            if (!$this->checkRoute($owen->copy(), $test_route)) {
                break;
            }

            array_push($route_orders, $order);
            array_push($exclude, $order->id);
            $curent = $order;
        }

        // все заказы которые подходят для доставки должны находиться в $route_orders
        return $this->createRoute($owen, $route_orders);
    }

    /**
     * Рассчитать и создать маршрут доставки
     * @param  Owen   Реальная печь
     * @param  array  $orders Массив из Order
     * @return App\Srvices\Logistic\Route
     */
    private function createRoute(Owen $owen, array $orders): Route
    {
        $route = new Route;
        $curent = new Point(0, 0); // начальная точка
        $start_at = $owen->bake($orders); // когда можно начать двигаться к следующей точке
        foreach ($orders as $order) {
            // время движения до следующей точки
            $move_time = $this->vehicle->moveTime($curent->distance($order->destination));
            $delivery_time = $start_at + $move_time; // когда заказ будет доставлен
            $order_delivery = new OrderDelivery($order, $delivery_time);
            $route->add($order_delivery);
            $order->setStatus(OrderStatus::INROUTE);
            $curent = $order->destination;
            $start_at = $delivery_time;
        }
        return $route;
    }

    /**
     * Отфильтрровать заказы имеющие статус NEWCREATED
     * и в диапазоне $max_delivery начиная с 1-го заказа в списке
     * @param array $orders массив App\Services\Order\Order
     * @return array App\Services\Order\Order
     */
    public function filterOrders(array $orders): array
    {
        $time = 0;
        $until = 0;
        $filtered = [];

        foreach ($orders as $order) {
            if ($order->getStatus() === OrderStatus::NEWCREATED) {
                // первая итерация
                if ($time === 0) {
                    $time = $order->created_at;
                    $until = $time + $this->max_delivery;
                }

                // время доставки текущего и последующих заказов больше нужного
                if ($order->created_at > $until) {
                    break;
                }

                array_push($filtered, $order);
            }
        }
        return $filtered;
    }

    /**
     * Отсортированная таблица приоритетов доставки заказов
     * заказы сортируются по параметрам: время доставки + время готовки
     * @param $owen Копия работающей печи
     * @param $orders Массив закзов
     * @param array $exclude Id заказов которые не включать в таблицу
     * @return
     */
    public function scoreTable(Owen $owen, Point $curent, array $orders, array $exclude = [])
    {
        // формат таблицы ключ: время готовности, значение: Order
        $scores = [];
        $curent = new Point();

        foreach ($orders as $order) {
            if (in_array($order->id, $exclude)) {
                continue;
            }
            $score = $this->orderScore($owen, $curent, $order);
            $scores[$score] = $order;
        }

        // Отсортировать в порядке убывания по времени готовности
        asort($scores);
        return $scores;
    }

    /**
     * Рассчитать приоритет отдельного заказ
     * @param  Owen   $owen   Копия печи
     * @param  Order  $order  Заказ
     * @param  Point  $curent Текущая точка
     * @return int Время готовности + доставка из текущей точки
     */
    public function orderScore(Owen $owen, Point $curent, Order $order): int
    {
        $distance = $curent->distance($order->destination);
        $delivery_time = $this->vehicle->moveTime($distance);

        // готовка + доставка
        return $order->bakeTime() + $delivery_time;
    }

    /**
     * Функция проверяет можно ли успеть
     * по такомоу маршруту всё доставить вовремя
     * @param  Owen   $owen   Копия печи
     * @param  array $orders Заказы в пути
     * @return [type]         [description]
     */
    public function checkRoute(Owen $owen, array $orders): bool
    {

        $curent = new Point(0, 0); // начальная точка
        $start_at = $owen->bake($orders); // когда можно начать двигаться к следующей точке

        foreach ($orders as $order) {
            // время движения до следующей точки
            $move_time = $this->vehicle->moveTime($curent->distance($order->destination));
            $delivery_time = $start_at + $move_time; // когда заказ будет доставлен

            // проверить будет ли товар доставлен за мин. время
            if ($delivery_time - $order->created_at < $this->max_delivery) {

                //echo ("delivery_time: {$delivery_time} created_at: {$order->created_at}" . "\n");

                return false;
            }

            $curent = $order->destination;
            $start_at = $delivery_time;
        }
        return true;
    }
}
