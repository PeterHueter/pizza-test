<?php

namespace App\Services\Owen;

use App\Services\Order\Order;
use App\Services\Owen\Contracts\Bakeable;
use Exception;

class Owen
{
    private $parallel;

    /**
     * Массив из одновременно готовящихся заказов
     * каждый элемент означает время когда освободится печь
     * @var array
     */
    private $queues = [];

    /**
     * [__construct description]
     * @param integer $parallel КОличество одновременно готовщихся заказов
     */
    public function __construct($parallel = 1)
    {
        if ($parallel < 1) {
            throw new Exception("Количество одновременно выпекакемых заказов не можеь быть меньше 1");

        }
        $this->parallel = $parallel;
        for ($i = 0; $i < $this->parallel; $i++) {
            $this->queues[$i] = 0;
        }
    }

    /**
     * Минимальное время когда будет доступна печь
     * @return int
     */
    public function freeAt(): int
    {
        return $this->queues[$this->minQueueIndex()];
    }

    /**
     * Время когда испекуться все заказы
     * @return int
     */
    public function doneAt(): int
    {
        $done = 0;
        foreach ($this->queues as $done_at) {
            $done = max($done, $done_at);
        }
        return $done;
    }

    /**
     * Индекс печи которая освободится раньше всех
     * @return int
     */
    private function minQueueIndex(): int
    {
        $index = 0;
        $free = $this->queues[$index];
        foreach ($this->queues as $idx => $freeTime) {
            if ($freeTime < $free) {
                $index = $idx;
                $free = $freeTime;
            }
        }
        return $index;
    }

    /**
     * Добавить заказы в печь
     * @param  array  $bakeables Массив из Order
     * @return int Время готовности всех заказов
     */
    public function bake(array $bakeables): int
    {
        $ready_at = 0;
        $min_start_at = 0; // начало выпечки
        foreach ($bakeables as $order) {
            if (!($order instanceof Order)) {
                throw new Exception("bake(...) ожидает экземпляр " . Order::class);
            }
            $min_start_at = max($min_start_at, $order->created_at);
        }

        foreach ($bakeables as $bakeable) {
            if (!($bakeable instanceof Bakeable)) {
                throw new Exception("bake(...) ожидает экземпляр " . Bakeable::class);
            }

            $index = $this->minQueueIndex();
            $start_at = max($this->queues[$index], $min_start_at);
            $this->queues[$index] = $start_at + $bakeable->bakeTime();
            $ready_at = max($ready_at, $this->queues[$index]);
        }
        return $ready_at;
    }

    /**
     * Возвращает копию
     * @return App\Services\Owen\Owen
     */
    public function copy()
    {
        return clone ($this);
    }
}
