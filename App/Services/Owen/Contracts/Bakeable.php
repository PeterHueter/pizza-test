<?php

namespace App\Services\Owen\Contracts;

interface Bakeable
{
    /**
     * Время готовки в минутах
     */
    public function bakeTime(): int;
}
