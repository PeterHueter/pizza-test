<?php

namespace App;

use App\Services\Logistic\Contracts\Service as LogisticService;
use App\Services\Order\FakeGenerator;
use App\Services\Order\Repository\Contracts\Repository as OrderRepository;

class Application
{
    private $fake; // генератор заказов
    private $repo; // хранилище заказов
    private $logistic; // сервис логистики

    public function __construct(
        FakeGenerator $fake,
        OrderRepository $repo,
        LogisticService $logistic
    ) {
        $this->fake = $fake;
        $this->repo = $repo;
        $this->logistic = $logistic;
    }

    public function run()
    {
        // Количество заказов
        $N = rand(10, 100);

        // Сгенерировать N заказов
        for ($i = 0; $i < $N; $i++) {
            $data = $this->fake->get();
            $this->repo->store($data['baketime'], $data['x'], $data['y'], $data['created_at']);
        }

        // вывести список заказов
        $orders = $this->repo->all();
        foreach ($orders as $order) {
            echo("{$order->id} {$order->created_at} {$order->bakeTime()} {$order->destination->x} {$order->destination->y} {$order->getStatus()}\n");
        }

        // сформировать маршруты
        $routes = $this->logistic->routes($orders);
        foreach ($routes as $route) {
            foreach ($route->getDelivery() as $delivery) {
                echo("{$delivery->order->id} {$delivery->when} ");
            }
            echo("\n");
        }
    }
}
