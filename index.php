<?php

require_once __DIR__ . '/vendor/autoload.php';

$fake = new App\Services\Order\FakeGenerator;
$repo = new App\Services\Order\Repository\InMemory;
$vehicle = new App\Services\Logistic\Vehicle(60);
$owen = new App\Services\Owen\Owen(2);
$logistic = new App\Services\Logistic\SimpleService($vehicle, $owen, 60);

$app = new \App\Application($fake, $repo, $logistic);
$app->run();
